
CREATE SEQUENCE adventure_order_id_seq;

CREATE TABLE adventure_order (
                id BIGINT NOT NULL DEFAULT nextval('adventure_order_id_seq'),
                adventure_event_id BIGINT NOT NULL,
                user_id BIGINT NOT NULL,
                order_date TIMESTAMP NOT NULL,
                payment_date TIMESTAMP,
                price NUMERIC(8,2) NOT NULL,
                CONSTRAINT adventure_order_pk PRIMARY KEY (id)
);


ALTER SEQUENCE adventure_order_id_seq OWNED BY adventure_order.id;
