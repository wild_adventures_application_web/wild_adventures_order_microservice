package com.wildadventures.microservice.order.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wildadventures.microservice.order.controller.EntityController;
import com.wildadventures.microservice.order.domain.Order;
import com.wildadventures.microservice.order.service.EntityService;

@RestController
@RequestMapping(path = "/orders", produces = "application/json")
public class OrderController implements EntityController<Order> {

	@Autowired
	private EntityService<Order> orderService;

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<Order> getOne(@PathVariable Long id) {
		Order order = this.orderService.findOne(id);

		return new ResponseEntity<Order>(order, HttpStatus.OK);
	}

	@GetMapping
	@Override
	public ResponseEntity<List<Order>> getMany(
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer size,
			@RequestParam(required = false, defaultValue = "false") boolean filter,
			@RequestParam(required = false) Long adventureEventId,
			@RequestParam(required = false) Long userId) {
		Order order = new Order();
		order.setAdventureEventId(adventureEventId);
		order.setUserId(userId);

		List<Order> orders = this.orderService.handleFind(page, size, filter, order);

		return new ResponseEntity<List<Order>>(orders, HttpStatus.OK);
	}

	@PostMapping
	@Override
	public ResponseEntity<Void> postMany(@RequestBody List<Order> orders) {
		this.orderService.insertMany(orders);

		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

	@PutMapping
	@Override
	public ResponseEntity<Void> putMany(@RequestBody List<Order> orders) {
		this.orderService.updateMany(orders);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@DeleteMapping
	@Override
	public ResponseEntity<Void> deleteMany(@RequestBody List<Order> orders) {
		this.orderService.deleteMany(orders);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}
}
