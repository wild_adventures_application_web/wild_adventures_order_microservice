package com.wildadventures.microservice.order.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface EntityController<T> {
	
	ResponseEntity<T> getOne(Long id);
	
	ResponseEntity<List<T>> getMany(Integer page, Integer size, boolean filter, Long adventureEventId, Long adventurerId);
	
	ResponseEntity<Void> postMany(List<T> entities);
	
	ResponseEntity<Void> putMany(List<T> entities);
	
	ResponseEntity<Void> deleteMany(List<T> entities);


}
