package com.wildadventures.microservice.order.domain;

import javax.persistence.*;

import lombok.Data;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the adventure_event database table.
 * 
 */
@Data
public class AdventureEvent extends BaseElement {

	private Timestamp date;
	private Adventure adventure;
	private List<Adventurer> adventurers;

	public AdventureEvent() {}	
	
	public AdventureEvent(Long id) {
		super(id);
	}	
	
}