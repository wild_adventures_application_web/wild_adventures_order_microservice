package com.wildadventures.microservice.order.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import lombok.Data;

@Data
public abstract class BaseElement {

	protected Long id;
	protected String description;
	protected String image;
	protected String name;
	
	@Transient
	protected List<Comment> comments = new ArrayList<Comment>();
	
	protected BaseElement() {}
	
	protected BaseElement(Long id) {
		this.id = id;
	}
}