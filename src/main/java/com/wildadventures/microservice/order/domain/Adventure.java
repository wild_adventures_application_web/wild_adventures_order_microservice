package com.wildadventures.microservice.order.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.List;

@Data
public class Adventure extends BaseElement {

	private Double price;
	private Category category;
	@Transient
	private List<AdventureEvent> adventureEvents;
	
	public Adventure() {}

	public Adventure(Long id) {
		super(id);
	}

}