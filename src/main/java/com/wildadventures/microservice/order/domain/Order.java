package com.wildadventures.microservice.order.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

import lombok.Data;

@Data
@Entity
@Table(name="adventure_order")
public class Order {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="adventure_event_id")
	private Long adventureEventId;
	
	@Column(name="user_id")
	private Long userId;
	
	@Column(name="order_date")
	private Timestamp orderDate;
	
	@Column(name="payment_date")
	private Timestamp paymentDate;
	
	private Double price;

}
