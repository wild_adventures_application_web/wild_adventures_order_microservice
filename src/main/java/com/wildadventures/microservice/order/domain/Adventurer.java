package com.wildadventures.microservice.order.domain;

import lombok.Data;

import java.util.List;

@Data
public class Adventurer {

	private Long id;

	private List<AdventureEvent> adventureEvents;

}