package com.wildadventures.microservice.order.domain;

import javax.persistence.*;

import lombok.Data;

import java.util.List;


/**
 * The persistent class for the category database table.
 * 
 */
@Data
public class Category extends BaseElement {
	
	@Transient
	private List<Adventure> adventures;

	public Category() {}
	
	public Category(Long id) {
		super(id);
	}

	
}