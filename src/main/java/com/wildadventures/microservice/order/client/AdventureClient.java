package com.wildadventures.microservice.order.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.wildadventures.microservice.order.domain.AdventureEvent;

@Repository
@FeignClient("adventure-microservice")
@RequestMapping("${adventure-microservice.context-path}")
public interface AdventureClient {

	@GetMapping(path = "/adventure-events/{id}")
	AdventureEvent getAdventureEvent(@PathVariable("id") Long id);
	
	@PutMapping(path = "/adventure-events")
	void updateAdventureEvents(@RequestBody List<AdventureEvent> adventureEvents);

}
