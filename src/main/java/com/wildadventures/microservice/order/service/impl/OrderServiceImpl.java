package com.wildadventures.microservice.order.service.impl;

import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.wildadventures.microservice.order.client.AdventureClient;
import com.wildadventures.microservice.order.domain.AdventureEvent;
import com.wildadventures.microservice.order.domain.Adventurer;
import com.wildadventures.microservice.order.domain.Order;
import com.wildadventures.microservice.order.exception.BadRequestException;
import com.wildadventures.microservice.order.exception.NotFoundException;
import com.wildadventures.microservice.order.repository.OrderRepository;
import com.wildadventures.microservice.order.service.EntityService;

@Service
public class OrderServiceImpl implements EntityService<Order> {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private AdventureClient adventureClient;
	
	Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Override
	public Order findOne(Long orderId) {
		Optional<Order> optOrder = this.orderRepository.findById(orderId);
		String errorMsg = String.format("No order with id %d.", orderId);

		return optOrder.orElseThrow(() -> new NotFoundException(errorMsg));
	}

	@Override
	public List<Order> handleFind(Integer page, Integer size, Boolean filter, Order entity) {
		List<Order> orders;

		ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase()
				.withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);

		if (!filter && (page == null || size == null)) {
			orders = this.orderRepository.findAll();
		} else if (!filter && (page != null && size != null)) {
			orders = this.orderRepository.findAll(PageRequest.of(page, size)).getContent();
		} else if (filter && (page == null || size == null)) {
			orders = this.orderRepository.findAll(Example.of(entity, matcher));
		} else {
			orders = this.orderRepository.findAll(Example.of(entity, matcher), PageRequest.of(page, size))
					.getContent();
		}
		
		if (orders.isEmpty()) {
			throw new NotFoundException("No orders found.");
		}
		
		return orders;
	}

	@Override
	public void insertMany(List<Order> orders) {
		String errorMsg;

		if (orders.isEmpty()) {
			errorMsg = "No orders provided.";

			throw new BadRequestException(errorMsg);
		}
		
		List<AdventureEvent> adventureEventsToUpdate = new ArrayList<>();

		for (Order order : orders) {
			if (order.getId() != null) {
				throw new BadRequestException("The ids are auto-generated, hint: use PUT to update an entity.");
			}
			
			long timeInMills = ZonedDateTime.now().toInstant().toEpochMilli();
			order.setOrderDate(new Timestamp(timeInMills));
			
			try {
				AdventureEvent adventureEvent = this.adventureClient.getAdventureEvent(order.getAdventureEventId());
				order.setPrice(adventureEvent.getAdventure().getPrice());
				
				Adventurer adventurer = new Adventurer();
				adventurer.setId(order.getUserId());
				adventureEvent.getAdventurers().add(adventurer);
				
				adventureEventsToUpdate.add(adventureEvent);
			} catch (Exception e){
				logger.error(ExceptionUtils.getStackTrace(e));
				throw new BadRequestException("An error occured while fetchig the adventureEvent assoicated to this order", e);
			}
		}
		
		this.adventureClient.updateAdventureEvents(adventureEventsToUpdate);

		this.orderRepository.saveAll(orders);
	}

	@Override
	public void updateMany(List<Order> orders) {
		String errorMsg;

		if (orders.isEmpty()) {
			errorMsg = "No orders provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Order order : orders) {
			if (order.getId() == null) {
				errorMsg = "An order has no Id. Hint: use POST to insert an entity.";
				throw new BadRequestException(errorMsg);
			} else if (!this.orderRepository.existsById(order.getId())) {
				errorMsg = String.format("No order with id %d.", order.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.orderRepository.saveAll(orders);
	}

	@Override
	public void deleteMany(List<Order> orders) {
		String errorMsg;

		if (orders.isEmpty()) {
			errorMsg = "No orders provided.";

			throw new BadRequestException(errorMsg);
		}

		for (Order order : orders) {
			if (order.getId() == null) {
				errorMsg = "An order has no Id. Cannot delete an order if no id is provided.";
				throw new BadRequestException(errorMsg);
			} else if (!this.orderRepository.existsById(order.getId())) {
				errorMsg = String.format("No order with id %d.", order.getId());
				throw new NotFoundException(errorMsg);
			}
		}

		this.orderRepository.deleteInBatch(orders);
	}
}
