package com.wildadventures.microservice.order.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wildadventures.microservice.order.domain.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

}
